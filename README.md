# Bitcoin Metrics

Real-time bitcoin transactions and blocks analysis using [Blockchain](https://www.blockchain.com) and [Coindesk](https://www.coindesk.com/) APIs.

### Prerequisites

The followings need to be installed on your computer :

* Docker
* Docker Compose
* Java 11 SDK
* Maven

### Installing

#### Docker-compose up
```
cd src/test/resources/docker
sudo docker-compose rm -svf
sudo docker-compose up --remove-orphans > /tmp/log
```
... and you're up with :
* a Zookeeper node instance
* a Kafka cluster composed of 3 nodes
* a [Kafka manager](http://localhost:9000) instance available
* a Storm cluster with a nimbus node and one supervisor
* a [storm-ui](http://localhost:8080) node
* an Elasticsearch cluster composed of 3 data nodes and one coordinating node
* a [Kibana](http://localhost:9200) instance
* one python dedicated node

### Build project

```
cd $PROJECT_ROOT
maven clean install
```

#### Docker erase all containers persistence :

Useful when restarting services (some persisted data such as cluster ids, topologies, ... are persisted and may prevent services to load correctly)

```
cd src/test/resources/docker
cd containers/ && sudo rm -rf ./kafka*_data/* ./nimbus/logs/* ./nimbus/data/* ./zookeeper_data/* ./supervisor/logs/* ./supervisor/data/* ./storm-ui/data/* ./storm-ui/logs/* ./es*/logs/* ./es*/repo/* ./es*/data/*
```

#### Docker re-build python image
In case some adjustments to the python Dockerfile (src/test/resources/docker/python/Dockerfile) are made, use the following to update the docker image.
```
sudo docker-compose build
```

#### Submit topology

Once compiled and the jar is built, the topology can be submitted to storm as follows :
```
sudo docker run --link nimbus --net docker_app-tier -it --rm -v $(pwd)/target/metrics-1.0-SNAPSHOT.jar:/metrics-1.0-SNAPSHOT.jar storm storm jar /metrics-1.0-SNAPSHOT.jar org.jmillet. bitcoin.metrics.BitcoinMetricsTopology
```

#### Launch XBT exchange rates retrieval
Exchange rates data can be obtained and pushed to Kafka using :
```
cd src/test/resources
sudo docker run -it --rm --name get-xbt-exchange-rates --link kafka --net docker_app-tier -v "/home/joseph/Documents/OpenClassroom/P3/workspace2/metrics:/usr/src/metrics" docker_python python /usr/src/metrics/src/main/resources/python/get_btc_exchange_rates.py
```

#### Launch BTC trx/block websocket
XBT transactions & block data can be obtained and pushed to Kafka using :
```
cd src/test/resources
sudo docker run -it --rm --name get-xbt-transactions --link kafka --net docker_app-tier -v "/home/joseph/Documents/OpenClassroom/P3/workspace2/metrics:/usr/src/metrics" docker_python python /usr/src/metrics/src/main/resources/python/get_btc_transactions.py
```
#### Storm ES persistence issue
https://github.com/elastic/elasticsearch-hadoop/issues/1357

## Running the tests

Explain how to run the automated tests for this system
```
mvn test
```

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

## Built With

* [Apache Kafka](https://kafka.apache.org/) - Message broker
* [Maven](https://maven.apache.org/) - Java dependency Management
* [Apache Storm](https://storm.apache.org/) - Real-time distributed computation
* [Elastic search](https://www.elastic.co/fr/elasticsearch/) - Distributed search engine
* [Kibana](https://www.elastic.co/fr/kibana) - Data visualization dashboard


## Author

* **Joseph Millet**