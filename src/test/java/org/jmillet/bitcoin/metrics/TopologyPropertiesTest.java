package org.jmillet.bitcoin.metrics;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.net.URL;

import org.apache.storm.Config;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Test;

public class TopologyPropertiesTest {

	@Test
	public void test() {


		URL configPath = this.getClass().getResource("/topology.config.properties");
		File configFile = new File(configPath.getFile());
		assertTrue(configFile.exists());
		
		TopologyProperties config = null;
		
		try {
			config = new TopologyProperties("/topology.config.properties");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		assertNotNull(config);
		String expectedHosts = "host1:2181,host2:2181";
		//String[] totot = config.getZookeeperHosts();
		assertEquals(expectedHosts, config.getZookeeperHosts());
		Config stormCfg = config.getStormConfig();
		assertTrue(Config.class.isInstance(stormCfg));

	}

}
