package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.Config;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class ValueExtractorBoltTest {

    private ValueExtractorBolt bolt;

    @Mock
    private TopologyContext topologyContext;

    @Mock
    private OutputCollector outputCollector;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void before() {
        bolt = new ValueExtractorBolt();
        bolt.prepare(new Config(), topologyContext, outputCollector);
    }

    @Test
    public void testExecute() throws IOException, URISyntaxException, ParseException {
        Tuple tuple = mock(Tuple.class);
        String msg = new String(getClass().getClassLoader().getResourceAsStream("transaction_msg.json").readAllBytes());
        when(tuple.getStringByField(BitcointMetricsUnitBolt.VALUE)).thenReturn(msg);

        bolt.execute(tuple);
        verify(outputCollector).emit(tuple, new Values(msg));
        verify(outputCollector).ack(tuple);
    }
}