package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.Config;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.mockito.Mockito.*;

public class BlockExchangeBoltTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    private BlockExchangeBolt bolt;
    @Mock
    private TopologyContext topologyContext;
    @Mock
    private OutputCollector outputCollector;

    @Before
    public void before() {
        bolt = new BlockExchangeBolt();
        bolt.prepare(new Config(), topologyContext, outputCollector);
    }

    @Test
    public void testExecute() {

        Tuple xchTuple = mock(Tuple.class);
        when(xchTuple.getStringByField(ExchangeBolt.QUOTE_CURRENCY_FIELD)).thenReturn("EUR");
        when(xchTuple.getDoubleByField(ExchangeBolt.EXCHANGE_RATE_FIELD)).thenReturn(Double.parseDouble("30000"));
        when(xchTuple.contains(ExchangeBolt.EXCHANGE_RATE_FIELD)).thenReturn(true);

        Tuple blkTuple = mock(Tuple.class);
        when(blkTuple.getStringByField(BlockBolt.FOUNDBY_FIELD)).thenReturn("Joseph");
        when(blkTuple.getStringByField(BlockBolt.DATETIME_FIELD)).thenReturn("2021-01-27T13:12:00+00:00");
        when(blkTuple.getStringByField(BlockBolt.HASH_FIELD)).thenReturn("0857b9de1884eec314ecf67c040a2657b8e083e1f95e31d0b5ba3d328841fc7f");
        when(blkTuple.getDoubleByField(BlockBolt.REWARD_FIELD)).thenReturn(Double.parseDouble("12.5"));
        when(blkTuple.contains(BlockBolt.REWARD_FIELD)).thenReturn(true);

        bolt.execute(xchTuple);
        bolt.execute(blkTuple);

        Values expected = new Values(
                "0857b9de1884eec314ecf67c040a2657b8e083e1f95e31d0b5ba3d328841fc7f",
                "2021-01-27T13:12:00+00:00",
                "Joseph",
                Double.parseDouble("12.5"),
                "EUR",
                Double.parseDouble("375000")
        );
        verify(outputCollector).ack(xchTuple);
        verify(outputCollector).emit(blkTuple, expected);
        verify(outputCollector).ack(blkTuple);
    }
}