package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.Config;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.mockito.Mockito.*;

public class TransactionBoltTest {

    private TransactionBolt bolt;

    @Mock
    private TopologyContext topologyContext;

    @Mock
    private OutputCollector outputCollector;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void before() {
        bolt = new TransactionBolt();
        bolt.prepare(new Config(), topologyContext, outputCollector);
    }

    @Test
    public void testExecute() throws IOException, URISyntaxException, ParseException {
        Tuple tuple = mock(Tuple.class);
        String msg = new String(getClass().getClassLoader().getResourceAsStream("transaction_msg.json").readAllBytes());
        when(tuple.getStringByField(TransactionBolt.VALUE)).thenReturn(msg);

        bolt.execute(tuple);

        Values expected = new Values(
                "2021-01-27T10:13:00+00:00",
                Double.parseDouble("50.15"),
                "0857b9de1884eec314ecf67c040a2657b8e083e1f95e31d0b5ba3d328841fc7f",
                Long.parseLong("1440086763")
        );
        verify(outputCollector).emit(tuple, expected);
        verify(outputCollector).ack(tuple);
    }
}