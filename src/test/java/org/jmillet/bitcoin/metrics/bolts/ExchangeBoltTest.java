package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.Config;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class ExchangeBoltTest {
    private ExchangeBolt bolt;

    @Mock
    private TopologyContext topologyContext;

    @Mock
    private OutputCollector outputCollector;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void before() {
        bolt = new ExchangeBolt();
        bolt.prepare(new Config(), topologyContext, outputCollector);
    }

    @Test
    public void testExecute() throws IOException, URISyntaxException, ParseException {
        Tuple tuple = mock(Tuple.class);
        String msg = new String(getClass().getClassLoader().getResourceAsStream("exchange_response.json").readAllBytes());
        when(tuple.getStringByField(ExchangeBolt.VALUE)).thenReturn(msg);

        bolt.execute(tuple);

        Values expected = new Values(
                "2021-01-27T13:12:00+00:00",
                "XBT",
                "EUR",
                Double.parseDouble("25502.5906"),
                "fe9d05f137bed75df75becd10a7954d7"
        );
        verify(outputCollector).emit(ExchangeBolt.XCH_STREAM, tuple, expected);
        verify(outputCollector).emit(ExchangeBolt.TRX_STREAM, tuple, expected);
        verify(outputCollector).emit(ExchangeBolt.BLK_STREAM, tuple, expected);
        verify(outputCollector).ack(tuple);
    }

}