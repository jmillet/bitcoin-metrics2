package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.Config;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class TransactionExchangeBoltTest {
    private TransactionExchangeBolt bolt;

    @Mock
    private TopologyContext topologyContext;

    @Mock
    private OutputCollector outputCollector;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Before
    public void before() {
        bolt = new TransactionExchangeBolt();
        bolt.prepare(new Config(), topologyContext, outputCollector);
    }

    @Test
    public void testExecute() throws IOException, URISyntaxException, ParseException {

        Tuple xchTuple = mock(Tuple.class);
        when(xchTuple.getStringByField(ExchangeBolt.QUOTE_CURRENCY_FIELD)).thenReturn("EUR");
        when(xchTuple.getDoubleByField(ExchangeBolt.EXCHANGE_RATE_FIELD)).thenReturn(Double.parseDouble("30000"));
        when(xchTuple.contains(ExchangeBolt.EXCHANGE_RATE_FIELD)).thenReturn(true);

        Tuple trxTuple = mock(Tuple.class);
        when(trxTuple.getDoubleByField(TransactionBolt.AMOUNT_FIELD)).thenReturn(Double.parseDouble("10.01"));
        when(trxTuple.getStringByField(TransactionBolt.DATETIME_FIELD)).thenReturn("2021-01-27T13:12:00+00:00");
        when(trxTuple.getStringByField(TransactionBolt.HASH_FIELD)).thenReturn("0857b9de1884eec314ecf67c040a2657b8e083e1f95e31d0b5ba3d328841fc7f");
        when(trxTuple.contains(TransactionBolt.AMOUNT_FIELD)).thenReturn(true);

        bolt.execute(xchTuple);
        bolt.execute(trxTuple);

        Values expected = new Values(
                "0857b9de1884eec314ecf67c040a2657b8e083e1f95e31d0b5ba3d328841fc7f",
                "2021-01-27T13:12:00+00:00",
                Double.parseDouble("10.01"),
                "EUR",
                Double.parseDouble("300300")
        );
        verify(outputCollector).ack(xchTuple);
        verify(outputCollector).emit(trxTuple, expected);
        verify(outputCollector).ack(trxTuple);
    }
}