package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.Config;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.io.IOException;
import java.net.URISyntaxException;

import static org.mockito.Mockito.*;

public class BlockBoltTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    private BlockBolt bolt;
    @Mock
    private TopologyContext topologyContext;
    @Mock
    private OutputCollector outputCollector;

    @Before
    public void before() {
        bolt = new BlockBolt();
        bolt.prepare(new Config(), topologyContext, outputCollector);
    }

    @Test
    public void testExecute() throws IOException, URISyntaxException, ParseException {
        Tuple tuple = mock(Tuple.class);
        String msg = new String(getClass().getClassLoader().getResourceAsStream("block_msg.json").readAllBytes());
        when(tuple.getStringByField(BlockBolt.VALUE)).thenReturn(msg);

        bolt.execute(tuple);

        Values expected = new Values(
                "2021-01-27T10:13:00+00:00",
                Double.parseDouble("12.5"),
                "00000000000006436073c07dfa188a8fa54fefadf571fd774863cda1b884b90f",
                Long.parseLong("1611742384"),
                "94e51495e0e8a0c3b78dac1220b2f35ceda8799b0a20cfa68601ed28126cfcc2"
        );
        verify(outputCollector).emit(tuple, expected);
        verify(outputCollector).ack(tuple);
    }
}