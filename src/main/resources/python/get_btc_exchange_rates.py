#!/usr/bin/env python

import hashlib
import json
import requests
import time
from kafka import KafkaProducer  # Run `pip install kafka-python` to install this package
from kazoo.client import KazooClient  # Run `pip install kazoo` to install this package

def get_kafka_producer(zookeeper_host):
    print("Connecting to zookeeper host : {}".format(zookeeper_host))
    brokers = get_kafka_broker(zookeeper_host, '')
    broker = next(brokers)
    print("Found kafka broker : {}".format(broker))
    producer = KafkaProducer(bootstrap_servers=broker)
    
    return producer


def get_kafka_broker(zk, path):
    zk = KazooClient(hosts=zk, read_only=True)
    zk.start()
    for node in zk.get_children(path + '/brokers/ids'):
        data, stats = zk.get(path + '/brokers/ids/' + node)
        props = json.loads(data)
        yield props['host'] + ':' + str(props['port'])
    zk.stop()



producer = get_kafka_producer('zookeeper:2181')

while True:
    response = requests.get("https://api.coindesk.com/v1/bpi/currentprice/EUR.json")
    data = response.json()
    hsh = hashlib.md5(("XBTEUR" + data["time"]["updatedISO"]).encode("utf-8")).hexdigest()
    msg = json.dumps({
        "base_currency": "XBT",
        "quote_currency": "EUR",
        "exchange_datetime": data["time"]["updatedISO"],
        # Storm expects a precision when parsing Double/Float
        "exchange_rate": data["bpi"]["EUR"]["rate_float"] / 100 * 100,
        "exchange_hash": hsh
    })
    print(msg)
    producer.send("xbt_xch", msg.encode("utf-8"),
                  key=str(hsh).encode())
    time.sleep(60)



  
    