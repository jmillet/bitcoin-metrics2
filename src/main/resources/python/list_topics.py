from kafka.admin import KafkaAdminClient, NewTopic


# admin_client = KafkaAdminClient(
#     bootstrap_servers=["localhost:9093"],
#     #api_version=(2, 7, 0),
#     client_id='test'
# )
#
# topic_list = []
# topic_list.append(NewTopic(name="btc_trx", num_partitions=3, replication_factor=2))
# topic_list.append(NewTopic(name="btc_blk", num_partitions=3, replication_factor=2))
# topic_list.append(NewTopic(name="btc_xch", num_partitions=3, replication_factor=2))
#
# admin_client.create_topics(new_topics=topic_list, validate_only=False)

import kafka
consumer = kafka.KafkaConsumer(group_id='test', bootstrap_servers=['localhost:9093'])
topics = consumer.topics()

print(topics)

