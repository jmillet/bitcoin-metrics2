#! /usr/bin/env python

# Connect to a websocket powered by blockchain.info and print events in
# the terminal in real time.

import json
from datetime import datetime
from time import time
import random
import websocket  # install this with the following command: pip install websocket-client
from kafka import KafkaProducer  # Run `pip install kafka-python` to install this package
from websocket import WebSocketConnectionClosedException
from kazoo.client import KazooClient  # Run `pip install kazoo` to install this package


def get_kafka_producer(zookeeper_host):
    print("Connecting to zookeeper host : {}".format(zookeeper_host))
    brokers = get_kafka_broker(zookeeper_host, '')
    broker = next(brokers)
    print("Found kafka broker : {}".format(broker))
    producer = KafkaProducer(bootstrap_servers=broker)
    
    return producer


def get_kafka_broker(zk, path):
    zk = KazooClient(hosts=zk, read_only=True)
    zk.start()
    for node in zk.get_children(path + '/brokers/ids'):
        data, stats = zk.get(path + '/brokers/ids/' + node)
        props = json.loads(data)
        yield props['host'] + ':' + str(props['port'])
    zk.stop()


producer = get_kafka_producer('zookeeper:2181')

miners = ['panicky', 'frightening', 'defective', 'glamorous', 'stupendous', 'vacuous', 'tenuous', 'savory', 'amusing', 'unadvised', 'ossified', 'dramatic', 'judicious', 'popular', 'abrasive', 'marvelous', 'imminent', 'imported', 'physical', 'recondite']
def main():
    ws = open_websocket_to_blockchain()
    
    last_ping_time = time()
    
    while True:
        # Receive event
        data = None
        try:
            data = json.loads(ws.recv())
        except json.decoder.JSONDecodeError:
            print("An invalid message was received ...")
            continue
        except (WebSocketConnectionClosedException, ConnectionResetError):
            print("A connection issue was detected, restarting websocket ...")
            ws = open_websocket_to_blockchain()
            continue
        
        # We ping the server every 10s to show we are alive
        if time() - last_ping_time >= 10:
            ws.send(json.dumps({"op": "ping"}))
            last_ping_time = time()
        
        # Response to "ping" events
        if data["op"] == "pong":
            pass
        
        # New unconfirmed transactions
        elif data["op"] == "utx":
            transaction_timestamp = data["x"]["time"]
            transaction_hash = data['x']['hash']  # this uniquely identifies the transaction
            transaction_total_amount = 0
            transaction_datetime = datetime.utcfromtimestamp(transaction_timestamp).replace(second=0).strftime(
                '%Y-%m-%dT%H:%M:%S+00:00')
            
            for recipient in data["x"]["out"]:
                # Every transaction may in fact have multiple recipients
                # Note that the total amount is in hundredth of microbitcoin; you need to
                # divide by 10**8 to obtain the value in bitcoins.
                transaction_total_amount += recipient["value"] / 100000000.
            
            print("{} New transaction {}: {} xbt".format(
                transaction_timestamp,
                transaction_hash,
                transaction_total_amount
            ))
            
            msg = json.dumps(
                {'transaction_timestamp': transaction_timestamp, 'transaction_datetime': transaction_datetime, 'transaction_hash': transaction_hash,
                 'transaction_total_amount': transaction_total_amount})
            print(msg)
            producer.send("xbt_trx", msg.encode(),
                          key=str(transaction_hash).encode())
        
        # New block
        elif data["op"] == "block":
            block_hash = data['x']['hash']
            block_timestamp = data["x"]["time"]
            block_datetime = datetime.utcfromtimestamp(block_timestamp).replace(second=0).strftime(
                '%Y-%m-%dT%H:%M:%S+00:00')
            # data["x"]["foundBy"]["description"] is empty, randomize it
            block_found_by = random.choice(miners)
            block_reward = 6.25 # blocks mined in 2020 have an associated reward of 6.25 xbt
            print("{} New block {} found by {}".format(block_datetime, block_hash, block_found_by))
            
            producer.send("xbt_blk", json.dumps(
                {'block_timestamp': block_timestamp, 'block_datetime': block_datetime, 'block_hash': block_hash,
                 'block_found_by': block_found_by,
                 'block_reward': block_reward}).encode(),
                          key=str(block_hash).encode())
        # This really should never happen
        else:
            print("Unknown op: {}".format(data["op"]))


def open_websocket_to_blockchain():
    print("Connecting to  wss://ws.blockchain.info/inv ...")
    # Open a websocket
    ws = websocket.WebSocket()
    ws.connect("wss://ws.blockchain.info/inv")
    # Register to unconfirmed transaction events
    ws.send(json.dumps({"op": "unconfirmed_sub"}))
    # Register to block creation events
    ws.send(json.dumps({"op": "blocks_sub"}))
    
    return ws


if __name__ == "__main__":
    main()
