package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseRichBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;

import java.util.Map;

public class BitcointMetricsUnitBolt extends BaseRichBolt {

    protected static final String KEY = "key";
    protected static final String VALUE = "value";
    protected static final Fields KEY_FIELDS = new Fields(KEY);
    protected static final Fields VALUE_FIELDS = new Fields(VALUE);
    protected OutputCollector outputCollector;

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        outputCollector = collector;
    }

    @Override
    public void execute(Tuple input) {
        try {
            process(input);
        } catch (ParseException | ClassCastException | IllegalArgumentException e) {
            // These exceptions are unlikely to be resolved when reprocessing tuple
            // Report error & discard message
            e.printStackTrace();
            outputCollector.reportError(e);
            outputCollector.ack(input);
        } catch (Exception e) {
            // We shouldn't normally come here
            // fail tuple, it will get reprocessed
            e.printStackTrace();
            outputCollector.reportError(e);
            outputCollector.fail(input);
        }
    }

    // Stub
    protected void process(Tuple input) throws ParseException, ClassCastException, IllegalArgumentException {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(VALUE_FIELDS);
    }

}
