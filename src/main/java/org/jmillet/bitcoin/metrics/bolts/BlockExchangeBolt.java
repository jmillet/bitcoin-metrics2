package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.Map;

public class BlockExchangeBolt extends BitcointMetricsUnitBolt {

    public static final String CONVERTED_AMOUNT_FIELD = "block_converted_reward";

    private double exchangeRate;
    private String quoteCurrency;

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        exchangeRate = Double.valueOf(0.0);
        quoteCurrency = "";
        super.prepare(topoConf, context, collector);
    }

    public void process(Tuple input) throws ParseException {
        if (input.contains(ExchangeBolt.EXCHANGE_RATE_FIELD)) {
            processExchange(input);
        } else if (input.contains(BlockBolt.REWARD_FIELD)) {
            processBlock(input);
        } else {
            throw new RuntimeException(String.format("Unrecognized tuple {%s}", input.toString()));
        }
    }

    private void processBlock(Tuple input) {
        Double convertedAmount = input.getDoubleByField(BlockBolt.REWARD_FIELD) * exchangeRate;
        outputCollector.emit(
                input,
                new Values(
                        input.getStringByField(BlockBolt.HASH_FIELD),
                        input.getStringByField(BlockBolt.DATETIME_FIELD),
                        input.getStringByField(BlockBolt.FOUNDBY_FIELD),
                        input.getDoubleByField(BlockBolt.REWARD_FIELD),
                        quoteCurrency,
                        convertedAmount
                ));
        outputCollector.ack(input);
    }

    private void processExchange(Tuple input) {
        exchangeRate = input.getDoubleByField(ExchangeBolt.EXCHANGE_RATE_FIELD);
        quoteCurrency = input.getStringByField(ExchangeBolt.QUOTE_CURRENCY_FIELD);
        outputCollector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(
                BlockBolt.HASH_FIELD,
                BlockBolt.DATETIME_FIELD,
                BlockBolt.FOUNDBY_FIELD,
                BlockBolt.REWARD_FIELD,
                ExchangeBolt.QUOTE_CURRENCY_FIELD,
                CONVERTED_AMOUNT_FIELD
        ));
    }
}
