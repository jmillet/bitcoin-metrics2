package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.shade.org.json.simple.JSONObject;
import org.apache.storm.shade.org.json.simple.parser.JSONParser;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class ExchangeBolt extends BitcointMetricsUnitBolt {

    public static final String DATETIME_FIELD = "exchange_datetime";
    public static final String BASE_CURRENCY_FIELD = "base_currency";
    public static final String QUOTE_CURRENCY_FIELD = "quote_currency";
    public static final String EXCHANGE_RATE_FIELD = "exchange_rate";
    public static final String HASH_FIELD = "exchange_hash";

    public static final String TRX_STREAM = "trx_stream";
    public static final String BLK_STREAM = "blk_stream";
    public static final String XCH_STREAM = "xch_stream";

    public void process(Tuple input) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject msg = (JSONObject) jsonParser.parse(input.getStringByField(VALUE));
        Values values = new Values(msg.get(DATETIME_FIELD), msg.get(BASE_CURRENCY_FIELD), msg.get(QUOTE_CURRENCY_FIELD), msg.get(EXCHANGE_RATE_FIELD), msg.get(HASH_FIELD));
        outputCollector.emit(TRX_STREAM, input, values);
        outputCollector.emit(BLK_STREAM, input, values);
        outputCollector.emit(XCH_STREAM, input, values);
        outputCollector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        Fields fields = new Fields(DATETIME_FIELD, BASE_CURRENCY_FIELD, QUOTE_CURRENCY_FIELD, EXCHANGE_RATE_FIELD, HASH_FIELD);
        declarer.declareStream(TRX_STREAM, fields);
        declarer.declareStream(BLK_STREAM, fields);
        declarer.declareStream(XCH_STREAM, fields);
    }
}

