package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

/**
 * Use to persist JSON in elastic search
 *
 * Map conf = new HashMap();
 * conf.put("es.input.json", "true");
 *
 * Only retain Tuple "value" field.
 *
 */
public class ValueExtractorBolt extends BitcointMetricsUnitBolt {
    @Override
    public void execute(Tuple input) {
        outputCollector.emit(input, new Values(input.getStringByField(VALUE)));
        outputCollector.ack(input);
    }
}
