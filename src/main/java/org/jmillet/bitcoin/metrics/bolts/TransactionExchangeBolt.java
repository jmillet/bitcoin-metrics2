package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.task.OutputCollector;
import org.apache.storm.task.TopologyContext;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.Map;

public class TransactionExchangeBolt extends BitcointMetricsUnitBolt {

    public static final String CONVERTED_AMOUNT_FIELD = "transaction_converted_total_amount";

    private double exchangeRate;
    private String quoteCurrency;

    @Override
    public void prepare(Map<String, Object> topoConf, TopologyContext context, OutputCollector collector) {
        exchangeRate = Double.valueOf(0.0);
        quoteCurrency = "";
        super.prepare(topoConf, context, collector);
    }

    public void process(Tuple input) throws ParseException {
        if (input.contains(ExchangeBolt.EXCHANGE_RATE_FIELD)) {
            processExchange(input);
        } else if (input.contains(TransactionBolt.AMOUNT_FIELD)) {
            processTransaction(input);
        } else {
            throw new RuntimeException(String.format("Unrecognized tuple {%s}", input.toString()));
        }
    }

    private void processTransaction(Tuple input) {
        double convertedAmount = input.getDoubleByField(TransactionBolt.AMOUNT_FIELD) * exchangeRate;
        outputCollector.emit(
                input,
                new Values(
                        input.getStringByField(TransactionBolt.HASH_FIELD),
                        input.getStringByField(TransactionBolt.DATETIME_FIELD),
                        input.getDoubleByField(TransactionBolt.AMOUNT_FIELD),
                        quoteCurrency,
                        convertedAmount
                ));
        outputCollector.ack(input);
    }

    private void processExchange(Tuple input) {
        exchangeRate = input.getDoubleByField(ExchangeBolt.EXCHANGE_RATE_FIELD);
        quoteCurrency = input.getStringByField(ExchangeBolt.QUOTE_CURRENCY_FIELD);
        outputCollector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(
                new Fields(
                        TransactionBolt.HASH_FIELD,
                        TransactionBolt.DATETIME_FIELD,
                        TransactionBolt.AMOUNT_FIELD,
                        ExchangeBolt.QUOTE_CURRENCY_FIELD,
                        CONVERTED_AMOUNT_FIELD
                )
        );
    }
}
