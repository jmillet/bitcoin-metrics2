package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.shade.org.json.simple.JSONObject;
import org.apache.storm.shade.org.json.simple.parser.JSONParser;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class BlockBolt extends BitcointMetricsUnitBolt {

    public static final String DATETIME_FIELD = "block_datetime";
    public static final String REWARD_FIELD = "block_reward";
    public static final String HASH_FIELD = "block_hash";
    public static final String TIMESTAMP_FIELD = "block_timestamp";
    public static final String FOUNDBY_FIELD = "block_found_by";

    public void process(Tuple input) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject msg = (JSONObject) jsonParser.parse(input.getStringByField(VALUE));

        outputCollector.emit(
                input,
                new Values(
                        msg.get(DATETIME_FIELD),
                        msg.get(REWARD_FIELD),
                        msg.get(HASH_FIELD),
                        msg.get(TIMESTAMP_FIELD),
                        msg.get(FOUNDBY_FIELD)
                )
        );
        outputCollector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(
                new Fields(
                        DATETIME_FIELD,
                        REWARD_FIELD,
                        HASH_FIELD,
                        TIMESTAMP_FIELD,
                        FOUNDBY_FIELD
                )
        );
    }
}
