package org.jmillet.bitcoin.metrics.bolts;

import org.apache.storm.shade.org.json.simple.JSONObject;
import org.apache.storm.shade.org.json.simple.parser.JSONParser;
import org.apache.storm.shade.org.json.simple.parser.ParseException;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

public class TransactionBolt extends BitcointMetricsUnitBolt {

    public static final String DATETIME_FIELD = "transaction_datetime";
    public static final String AMOUNT_FIELD = "transaction_total_amount";
    public static final String HASH_FIELD = "transaction_hash";
    public static final String TIMESTAMP_FIELD = "transaction_timestamp";

    public void process(Tuple input) throws ParseException {
        JSONParser jsonParser = new JSONParser();
        JSONObject msg = (JSONObject) jsonParser.parse(input.getStringByField(VALUE));
        outputCollector.emit(
                input,
                new Values(
                        msg.get(DATETIME_FIELD),
                        msg.get(AMOUNT_FIELD),
                        msg.get(HASH_FIELD),
                        msg.get(TIMESTAMP_FIELD)
                )
        );
        outputCollector.ack(input);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(
                new Fields(
                        DATETIME_FIELD,
                        AMOUNT_FIELD,
                        HASH_FIELD,
                        TIMESTAMP_FIELD
                )
        );
    }
}
