package org.jmillet.bitcoin.metrics;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.storm.Config;
import org.apache.storm.LocalCluster;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.StormTopology;
import org.apache.storm.kafka.spout.KafkaSpout;
import org.apache.storm.kafka.spout.KafkaSpoutConfig;
import org.apache.storm.topology.TopologyBuilder;
import org.elasticsearch.storm.EsBolt;
import org.jmillet.bitcoin.metrics.bolts.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class BitcoinMetricsTopology {

    public static final Logger LOG = LoggerFactory.getLogger(BitcoinMetricsTopology.class);
    private static final String KAFKA_BITCOIN_TRANSACTIONS_TOPIC = "xbt_trx";
    private static final int KAFKA_BITCOIN_TRANSACTIONS_PARRALELISM_HINT = 4;
    private static final String KAFKA_BITCOIN_BLOCKS_TOPIC = "xbt_blk";
    private static final int KAFKA_BITCOIN_BLOCKS_PARRALELISM_HINT = 1;
    private static final String KAFKA_BITCOIN_EXCHANGE_TOPIC = "xbt_xch";
    private static final int KAFKA_BITCOIN_EXCHANGE_PARRALELISM_HINT = 1;
    private static final String KAFKA_GROUP = "xbt";
    private final TopologyProperties topologyProperties;

    public BitcoinMetricsTopology(TopologyProperties topologyProperties) {
        this.topologyProperties = topologyProperties;
    }

    public static void main(String[] args) throws Exception {
        // only support topology.config.properties files
        TopologyProperties topologyProperties = new TopologyProperties("/META-INF/topology.config.properties");
        BitcoinMetricsTopology topology = new BitcoinMetricsTopology(topologyProperties);
        topology.runTopology();
    }

    public void runTopology() throws Exception {

        StormTopology stormTopology = buildTopology();
        String stormExecutionMode = topologyProperties.getStormExecutionMode();

        switch (stormExecutionMode) {
            case ("cluster"):
                StormSubmitter.submitTopology(topologyProperties.getTopologyName(), topologyProperties.getStormConfig(),
                        stormTopology);
                break;
            case ("local"):
            default:
                LocalCluster cluster = new LocalCluster();
                cluster.submitTopology(topologyProperties.getTopologyName(), topologyProperties.getStormConfig(),
                        stormTopology);
                Thread.sleep(topologyProperties.getLocalTimeExecution());
                cluster.killTopology(topologyProperties.getTopologyName());
                cluster.shutdown();
                System.exit(0);
        }
    }

    private StormTopology buildTopology() {
        TopologyBuilder builder = new TopologyBuilder();

        setExchangeDag(builder);
        setTransactionDag(builder);
        setBlockDag(builder);

        return builder.createTopology();
    }

    private void setExchangeDag(TopologyBuilder builder) {
        // XBT exchange rates DAG
        KafkaSpoutConfig kafkaCfg = getKafkaSpoutConfig(KAFKA_BITCOIN_EXCHANGE_TOPIC);
        builder.setSpout("xch_spout", new KafkaSpout<>(kafkaCfg));
        builder.setBolt("xch_bolt", new ExchangeBolt())
                .shuffleGrouping("xch_spout");

        Map confXch = new HashMap();
        confXch.put("es.mapping.id", ExchangeBolt.HASH_FIELD);
        confXch.put("es.storm.bolt.flush.entries.size", 1); // 1 every minute, flush them once we get them

        builder.setBolt(
                "es-xch-bolt",
                new EsBolt(
                        "xbt_xch",
                        confXch
                ),
                KAFKA_BITCOIN_EXCHANGE_PARRALELISM_HINT)
                //.addConfiguration(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 10) // 6 times per minute
                .shuffleGrouping("xch_bolt", ExchangeBolt.XCH_STREAM);
    }

    private void setTransactionDag(TopologyBuilder builder) {
        // XBT transactions DAG
        KafkaSpoutConfig kafkaCfg = getKafkaSpoutConfig(KAFKA_BITCOIN_TRANSACTIONS_TOPIC);
        builder.setSpout("trx_spout", new KafkaSpout<>(kafkaCfg));
        builder.setBolt("trx_bolt", new TransactionBolt())
                .shuffleGrouping("trx_spout");

        builder.setBolt("trx_with_xch_bolt", new TransactionExchangeBolt())
                .shuffleGrouping("trx_bolt")
                .globalGrouping("xch_bolt", ExchangeBolt.TRX_STREAM);

        Map confTrx = new HashMap();
        confTrx.put("es.mapping.id", TransactionBolt.HASH_FIELD);
        confTrx.put("es.storm.bolt.flush.entries.size", "50"); // Flush trx / 50 trx

        builder.setBolt(
                "es-trx-bolt",
                new EsBolt(
                        "xbt_trx",
                        confTrx
                ),
                KAFKA_BITCOIN_TRANSACTIONS_PARRALELISM_HINT)
                //.addConfiguration(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 2)
                .shuffleGrouping("trx_with_xch_bolt");
    }

    private void setBlockDag(TopologyBuilder builder) {
        // XBT blocks DAG
        KafkaSpoutConfig kafkaCfg = getKafkaSpoutConfig(KAFKA_BITCOIN_BLOCKS_TOPIC);
        builder.setSpout("blk_spout", new KafkaSpout<>(kafkaCfg));
        builder.setBolt("blk_bolt", new BlockBolt())
                .shuffleGrouping("blk_spout");
        builder.setBolt("blk_with_xch_bolt", new BlockExchangeBolt())
                .shuffleGrouping("blk_bolt")
                .globalGrouping("xch_bolt", ExchangeBolt.BLK_STREAM);

        Map confBlk = new HashMap();
        confBlk.put("es.mapping.id", BlockBolt.HASH_FIELD);
        confBlk.put("es.storm.bolt.flush.entries.size", "1"); // blocks are kind of rare, flush them once we get them

        builder.setBolt(
                "es-blk-bolt",
                new EsBolt(
                        "xbt_blk",
                        confBlk
                ),
                KAFKA_BITCOIN_BLOCKS_PARRALELISM_HINT)
                //.addConfiguration(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, 10) // 6 times per minute
                .shuffleGrouping("blk_with_xch_bolt");
    }

    private KafkaSpoutConfig getKafkaSpoutConfig(String topic) {
        return KafkaSpoutConfig
                .builder(topologyProperties.getZookeeperHosts(), topic)
                .setProp(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, topologyProperties.getKafkaBrokerHosts())
                .setProp(ConsumerConfig.GROUP_ID_CONFIG, KAFKA_GROUP)
                .build();
    }
}
