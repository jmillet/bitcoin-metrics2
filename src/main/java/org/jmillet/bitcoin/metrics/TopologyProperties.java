package org.jmillet.bitcoin.metrics;

import org.apache.storm.Config;

import org.apache.storm.metric.LoggingMetricsConsumer;

import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class TopologyProperties {


	private String topologyName;
	private int localTimeExecution, kafkaSpoutParallelism, filterBoltParallelism, tcpBoltParallelism;
	private Config stormConfig;
	private String esHosts;

	private String zookeeperHosts;
	private String stormExecutionMode;

	private String kafkaBrokerHosts;
	private boolean kafkaStartFromBeginning;


	public TopologyProperties(String fileName) {

		stormConfig = new Config();

		try {
			setProperties(fileName);
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}
	}

	

	private Properties readPropertiesFile(String fileName) throws Exception {
		Properties properties = new Properties();
		URL url = this.getClass().getResource(fileName);		
		if (url == null) {
			throw new ConfigurationException("Cannot find configuration file !");
		}
		properties.load(url.openStream());
		
		return properties;
	}

	private void setProperties(String fileName) throws Exception {

		Properties properties = readPropertiesFile(fileName);
		topologyName = properties.getProperty("storm.topology.name", "defaultTopologyName");
		localTimeExecution = Integer.parseInt(properties.getProperty("storm.local.execution.time", "20000"));
		kafkaSpoutParallelism = Integer.parseInt(properties.getProperty("kafka.spout.paralellism", "1"));
		kafkaBrokerHosts = properties.getProperty("kafka.broker.hosts");

		kafkaStartFromBeginning = Boolean.parseBoolean(properties.getProperty("kafka.startFromBeginning", "false"));
		setStormConfig(properties);
	}

	private void setStormConfig(Properties properties) throws ConfigurationException {

		stormExecutionMode = properties.getProperty("storm.execution.mode", "local");
		int stormWorkersNumber = Integer.parseInt(properties.getProperty("storm.workers.number", "2"));
		int maxTaskParallism = Integer.parseInt(properties.getProperty("storm.max.task.parallelism", "2"));

		zookeeperHosts = properties.getProperty("zookeeper.hosts");
		if (zookeeperHosts == null) {
			throw new ConfigurationException("Zookeeper hosts must be specified in configuration file");
		}

		int topologyBatchEmitMillis = Integer
				.parseInt(properties.getProperty("storm.topology.batch.interval.miliseconds", "2000"));
		String nimbusHost = properties.getProperty("storm.nimbus.host", "localhost");
		String nimbusPort = properties.getProperty("storm.nimbus.port", "6627");

		// How often a batch can be emitted in a Trident topology.
		//stormConfig.put(Config.TOPOLOGY_TRIDENT_BATCH_EMIT_INTERVAL_MILLIS, topologyBatchEmitMillis);
		stormConfig.setNumWorkers(stormWorkersNumber);
		stormConfig.setMaxTaskParallelism(maxTaskParallism);
		// Storm cluster specific properties
		stormConfig.put(Config.NIMBUS_SEEDS, Arrays.asList(nimbusHost.split(",")));
		stormConfig.put(Config.NIMBUS_THRIFT_PORT, Integer.parseInt(nimbusPort));
		stormConfig.put(Config.STORM_ZOOKEEPER_PORT, parseZkPort(zookeeperHosts));
		stormConfig.put(Config.STORM_ZOOKEEPER_SERVERS, parseZkHosts(zookeeperHosts));
		stormConfig.put(Config.TOPOLOGY_MAX_SPOUT_PENDING, Integer.parseInt(properties.getProperty("topology.max.spout.pending", "5000")));
		stormConfig.put(Config.TOPOLOGY_MESSAGE_TIMEOUT_SECS, Integer.parseInt(properties.getProperty("topology.message.timeout.secs", "30")));
		// Debug Tuples
		stormConfig.put(Config.TOPOLOGY_EVENTLOGGER_EXECUTORS, 10);

		// Elastic search configuration
		stormConfig.put("es.nodes", properties.getProperty("elasticsearch.hosts"));
		stormConfig.put("es.port", properties.getProperty("elasticsearch.port"));
		stormConfig.put("es.index.auto.create", Boolean.parseBoolean(properties.getProperty("es.index.auto.create", "true")));
		stormConfig.put("es.storm.bolt.write.ack", Boolean.parseBoolean(properties.getProperty("es.storm.bolt.write.ack", "false")));

		// Causes "EsHadoopIllegalArgumentException: Cannot flush non-initialized write operation" exceptions when ticks comes and no data is here
		// cf https://github.com/elastic/elasticsearch-hadoop/issues/1357
		stormConfig.put("es.storm.bolt.tick.tuple.flush", "false");
		stormConfig.put(Config.TOPOLOGY_DEBUG, Boolean.parseBoolean(properties.getProperty("topology.debug", "false")));

		// Kafka configuration
		stormConfig.put("kafka.broker.hosts", properties.getProperty("kafka.broker.hosts"));



		// register metric consumer
		// stormConfig.registerMetricsConsumer(JMXMetricConsumer.class, 1);
		stormConfig.registerMetricsConsumer(LoggingMetricsConsumer.class, 1);
	}

	private static int parseZkPort(String zkNodes) {
		String[] hostsAndPorts = zkNodes.split(",");
		int port = Integer.parseInt(hostsAndPorts[0].split(":")[1]);
		return port;
	}

	private static List<String> parseZkHosts(String zkNodes) {

		String[] hostsAndPorts = zkNodes.split(",");
		List<String> hosts = new ArrayList<String>(hostsAndPorts.length);

		for (int i = 0; i < hostsAndPorts.length; i++) {
			hosts.add(i, hostsAndPorts[i].split(":")[0]);
		}
		return hosts;
	}


	public String getKafkaBrokerHosts() {
		return kafkaBrokerHosts;
	}

	public String getTopologyName() {
		return topologyName;
	}

	public int getLocalTimeExecution() {
		return localTimeExecution;
	}

	public Config getStormConfig() {
		return stormConfig;
	}

	public String getZookeeperHosts() {
		return zookeeperHosts;
	}

	public String getStormExecutionMode() {
		return stormExecutionMode;
	}

	public boolean isKafkaStartFromBeginning() {
		return kafkaStartFromBeginning;
	}

	public int getKafkaSpoutParallelism() {
		return kafkaSpoutParallelism;
	}

	public int getFilterBoltParallelism() {
		return filterBoltParallelism;
	}

	public int getTcpBoltParallelism() {
		return tcpBoltParallelism;
	}

	@Override
	public String toString() {
		return "TopologyProperties{" +
				"topologyName='" + topologyName + '\'' +
				", localTimeExecution=" + localTimeExecution +
				", kafkaSpoutParallelism=" + kafkaSpoutParallelism +
				", filterBoltParallelism=" + filterBoltParallelism +
				", tcpBoltParallelism=" + tcpBoltParallelism +
				", stormConfig=" + stormConfig +
				", esHosts='" + esHosts + '\'' +
				", zookeeperHosts='" + zookeeperHosts + '\'' +
				", stormExecutionMode='" + stormExecutionMode + '\'' +
				", kafkaBrokerHosts='" + kafkaBrokerHosts + '\'' +
				", kafkaStartFromBeginning=" + kafkaStartFromBeginning +
				'}';
	}
}
